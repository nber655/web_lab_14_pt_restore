// Allow us to use the Express frameworks
var express = require('express');

// Setup a new Express app
var app = express();

// The app should listen on port 3000, unless a different
// port is specified in the environment.
app.set('port', process.env.PORT || 3000);

// Specify that the app should use handlebars
var handlebars = require('express-handlebars');
app.engine('handlebars', handlebars({ defaultLayout: 'main' }));
app.set('view engine', 'handlebars');

// Specify that the app should use body parser (for reading submitted form POST data)
var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));

// Specify that when we browse to "/" with a GET request, render the
// questionnaire/questionnaire-form view
app.get('/', function (req, res) {

    res.render("questionnaire/questionnaire-form", { pageTitle: "Ex 02 Form" });
});

// TODO Step 1. Add a route handler for POST requests to "/submission".

// Specify that when we browse to "/submission" with a POST request, read the submitted form values and then render echo-service-submission.handlebars.
app.post('/submission', function (req, res) {// handeling the form. Reading the host data

    // Read the form data
    var data = {
        pageTitle: "Echo Service Submission",
        entries: req.body 
    };

    res.render("echo-service/echo-service-submission", data);
});


// Exercise THREE: Extending the Echo Service
// Specify that when we browse to "/getform" with a GET request, render the
// questionnaire/questionnaire-form view
app.get('/getform', function (req, res) {// handeling the form. Reading the host data

    // Read the form data
    var data = {
        useGet: true,
        pageTitle: "Ex 02 Form",
        entries: req.query 
    };

    res.render("questionnaire/questionnaire-form", data);
});


// Specify that when we browse to "/" with a GET request, read the submitted form values and then render echo-service-submission.handlebars.
app.get('/getform/submission', function (req, res) {// handeling the form. Reading the host data

    // Read the form data
    var data = {
        pageTitle: "Ex 02 Form",
        entries: req.query 
    };

    res.render("echo-service/echo-service-submission", data);
});

// Allow the server to serve up files from the "public" folder.
app.use(express.static(__dirname + "/public"));

// Start the server running.
app.listen(app.get('port'), function () {
    console.log('Express started on http://localhost:' + app.get('port'));
});