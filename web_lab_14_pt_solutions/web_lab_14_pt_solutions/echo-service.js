// Allow us to use the Express frameworks
var express = require('express');

// Setup a new Express app
var app = express();

// The app should listen on port 3000, unless a different
// port is specified in the environment.
app.set('port', process.env.PORT || 3000);

// Specify that the app should use handlebars
var handlebars = require('express-handlebars');
app.engine('handlebars', handlebars({ defaultLayout: 'main' }));
app.set('view engine', 'handlebars');

// Specify that the app should use body parser (for reading submitted form POST data)
var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));

// Specify that when we browse to "/" with a GET request, render the
// questionnaire/questionnaire-form view
app.get('/', function (req, res) {

    res.render("questionnaire/questionnaire-form", { pageTitle: "Ex 02 Form" });
});

// Specify that when we browse to "/getform" with a GET request, render the
// questionnaire/questionnaire-form view, with useGet = true.
app.get('/getform', function (req, res) {

    res.render("questionnaire/questionnaire-form", { pageTitle: "Ex 03 Form", useGet: true });
});

// Extracted most of the shared stuff out into a function to reduce code duplication.
// Renders the echo-service/echo-service-submission view with the provided form submission values
// (from either a req.body or req.query).
function renderFormSubmissionPage(formSubmissions, res) {
    // Read the query data
    // var entries = [];
    // for (var key in formSubmissions) {
    //     var entry = { key: key, value: formSubmissions[key] };
    //     entries[entries.length] = entry;
    // }

    var data = {
        pageTitle: "Echo Service Submission",
        entries: formSubmissions
    };

    res.render("echo-service/echo-service-submission", data);
}

// Specify that when we browse to "/submission" with a GET request, read the submitted query values
// and then render the submission page.
app.get('/submission', function (req, res) {

    renderFormSubmissionPage(req.query, res);
});

// Specify that when we browse to "/submission" with a POST request, read the submitted form values
// and then render the submission page.
app.post('/submission', function (req, res) {

    renderFormSubmissionPage(req.body, res);
});

// Allow the server to serve up files from the "public" folder.
app.use(express.static(__dirname + "/public"));

// Start the server running.
app.listen(app.get('port'), function () {
    console.log('Express started on http://localhost:' + app.get('port'));
});