// Allow us to use the Express framework
var express = require('express');

// Specify that the app should use Formidable (to process file uploads)
var formidable = require("formidable");

// Specify that the app should use Jimp (to create image thumbnails)
var jimp = require("jimp");

// Specify that the app should use fs (to scan directory contents)
var fs = require("fs");

// Setup a new Express app
var app = express();

// The app should listen on port 3000, unless a different
// port is specified in the environment.
app.set('port', process.env.PORT || 3000);

// Specify that the app should use handlebars
var handlebars = require('express-handlebars');
app.engine('handlebars', handlebars({ defaultLayout: 'main' }));
app.set('view engine', 'handlebars');

// Reads all the images in the public/images/thumbnails folder, then
// renders the image-gallery/image-gallery view.
function renderImageGallery(req, res) {

    fs.readdir(__dirname + "/public/images/thumbnails/", function (err, files) {

        var gallery = [];
        for (var i = 0; i < files.length; i++) {
            var file = files[i].toLowerCase();
            if (file.endsWith(".png") || file.endsWith(".bmp") ||
                file.endsWith(".jpg") || file.endsWith(".jpeg")) {

                gallery[gallery.length] = file;
            }
        }

        var data = {
            pageTitle: "Image Gallery",
            gallery: gallery
        }

        res.render("image-gallery/image-gallery", data);

    });
}

// Specify that when we browse to "/" with a GET request, render the image gallery.
app.get('/', function (req, res) {
    renderImageGallery(req, res);
});

// Specify that when we browse to "/" with a POST request, process the file upload, then render the image gallery.
app.post('/', function (req, res) {

    var form = new formidable.IncomingForm();

    // Save uploaded files in the ./public/images folder.
    form.on("fileBegin", function (name, file) {
        file.path = __dirname + "/public/images/fullsize/" + file.name;
    });

    form.parse(req, function (err, fields, files) {

        // Get the file which was uploaded.
        var file = files.fileUpload;

        // Create a thumbnail for the file - max. 400x400px.
        // Read the uploaded image from disk
        jimp.read(__dirname + "/public/images/fullsize/" + file.name, function (err, image) {

            // When the image has been read, create its thumbnail by scaling it to fit inside a
            // 400x400 box, then save it.
            image
                .scaleToFit(400, 400)
                .write(__dirname + "/public/images/thumbnails/" + file.name, function (err) {

                    // When the thumbnail has been saved, then render the image gallery.
                    renderImageGallery(req, res);

                });
        });

    });
});


// Allow the server to serve up files from the "public" folder.
app.use(express.static(__dirname + "/public"));

// Start the server running.
app.listen(app.get('port'), function () {
    console.log('Express started on http://localhost:' + app.get('port'));
});