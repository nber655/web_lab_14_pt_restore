// Allow us to use the Express frameworks
var express = require('express');

// Setup a new Express app
var app = express();

// The app should listen on port 3000, unless a different
// port is specified in the environment.
app.set('port', process.env.PORT || 3000);

// Specify that the app should use handlebars // Enabeling the use of handlebars with "apps"
var handlebars = require('express-handlebars');
app.engine('handlebars', handlebars({ defaultLayout: 'main' }));
app.set('view engine', 'handlebars');

// Specify that the app should use body parser (for reading submitted form data)
var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true })); // Allows us to read forms submitted with POST requests

// Specify that when we browse to "/" with a GET request, show the example01/form view
app.get('/', function (req, res) {

    res.render("example01/form");
});

// Specify that when we browse to "/" with a POST request, read the submitted form values and then render submitForm.handlebars.
app.post('/', function (req, res) {// handeling the form. Reading the host data

    // Read the form data
    var data = {
        name: req.body.name, // refering to the request body. Name is the name attribute of the input control located in form.hadlebars // you can also do name: req.body["name"]
        options: req.body.options // Options is the name attribute of the checkboxes control located in form.hadlebars 
    };

    res.render("example01/submitForm", data);
});

/* We haven't specified any other routes, so browsing anywhere other than "/" will result in a default error page being returned. */

// Start the server running.
app.listen(app.get('port'), function () {
    console.log('Express started on http://localhost:' + app.get('port'));
});